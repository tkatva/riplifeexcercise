package tkatva.riplife.exercise.test;

import org.junit.Before;
import org.junit.Test;
import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.parsers.CommandLineParser;
import tkatva.riplife.exercise.parsers.InputStringParser;
import tkatva.riplife.exercise.parsers.ParserFactory;
import tkatva.riplife.exercise.parsers.WhitespaceInputStringParser;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class TestParsers {

    InputStringParser whiteSpaceParser;
    CommandLineParser commandLineParser;

    static final String CIRCLE_COMMAND_STRING = "store circle 0.1 0.1 100";

    @Before
    public void setUp() {

        whiteSpaceParser = new WhitespaceInputStringParser();
        commandLineParser = ParserFactory.getCommandLineParser();

    }

    @Test
    public void testCircleCommandParse() {

        String[] parameters = whiteSpaceParser.parseInputString(CIRCLE_COMMAND_STRING);

        assertEquals(5,parameters.length);

    }

    @Test
    public void testCommandLineParser() {

        List<CommandAction> commands = commandLineParser.parseCommand(CIRCLE_COMMAND_STRING);
        assertTrue( (commands != null && commands.size() == 1) );


    }

}
