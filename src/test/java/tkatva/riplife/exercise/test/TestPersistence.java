package tkatva.riplife.exercise.test;

import org.junit.*;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.parsers.FileHandler;
import tkatva.riplife.exercise.parsers.FileHandlerFactory;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.persistence.ShapeStore;
import tkatva.riplife.exercise.persistence.ShapeStoreHazelcastImpl;
import tkatva.riplife.exercise.shapes.*;

import java.net.URL;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tuomas Katva on 7.6.2014.
 *
 * This test should be refactored and broken to pieces
 *
 */
public class TestPersistence {

    private static ShapeStore shapeStore = null;

    private static final double circle1X = 0.1;
    private static final double circle1Y = 0.1;
    private static final double circle1Radius = 100;
    private static final String circle1ID = "1";

    private static double circle2X = 0.2;
    private static double circle2Y = 0.3;
    private static double circle2Radius = 1000;
    private static double donutOuterRadius = 5000;
    private static final String circle2ID = "2";

    private static double commonX = 0.1;
    private static double commonY = 0.12;

    private static double rectangleSide1 = 100;
    private static double getRectangleSide2 = 120;

    private static String rect1ID = "rect1";

    private static String squareID = "square1";

    private static String donutID = "donut1";

    @BeforeClass
    public static void initPersistenceStore() {
        shapeStore = PersistenceStoreFactory.getDefaultShapeStore();
        insertTestData();
    }

    @Before
    public void setUp() {




    }

    @AfterClass
    public static void tearDown() {
        PersistenceStoreFactory.getDefaultShapeStore().shutDown();
    }

    private static void insertTestData() {

        Circle circle1 = new Circle(new CirclePointChecker(circle1X,circle1Y,circle1Radius), Optional.of(circle1ID));
        circle1.setX(circle1X);
        circle1.setY(circle1Y);
        circle1.setRadius(circle1Radius);

        Circle circle2 = new Circle(new CirclePointChecker(circle2X,circle2Y,circle2Radius), Optional.of(circle2ID));
        circle2.setX(circle2X);
        circle2.setY(circle2Y);
        circle2.setRadius(circle2Radius);

        Rectangle rect = new Rectangle(new RectanglePointChecker(commonX,commonY,rectangleSide1,getRectangleSide2),Optional.of(rect1ID));
        rect.setSide1(rectangleSide1);
        rect.setSide2(getRectangleSide2);
        rect.setX(commonX);
        rect.setY(commonY);

        Square square = new Square(new RectanglePointChecker(commonX,commonY,rectangleSide1,rectangleSide1),Optional.of(squareID));
        square.setX(commonX);
        square.setY(commonY);
        square.setSide(rectangleSide1);

        Donut donut = ShapeFactory.getDonut(commonX,commonY,circle2Radius,donutOuterRadius);


        shapeStore.addShape(circle1);
        shapeStore.addShape(circle2);
        shapeStore.addShape(rect);
        shapeStore.addShape(square);
        shapeStore.addShape(donut);


    }

    @Test
    public void testFindWithId() {

        Optional<Shape> shape = shapeStore.findShapeById(circle1ID);

        assertTrue("Could not find shape by ID : " + circle1ID,shape.isPresent());

    }

    @Test
    public void testFindWithXY() {

        List<Shape> shapes = shapeStore.findShapesWithYandYcoordinates(0.2,0.2);

        assertEquals(4,shapes.size());


    }

    @Test
    public void testFindDonut() {

        List<Shape> shapes = shapeStore.findShapesWithYandYcoordinates(3050,3050);
     /*   assertTrue(shapes.size()>1);*/
        assertEquals(1,shapes.size());

    }

    @Test
    public void testFileRead() {

        FileHandler fileHandler = FileHandlerFactory.getDefaultFileHandler();
        URL resourceUrl = getClass().
                getResource("/testfile.txt");
        List<ActionResult> results = fileHandler.processFile(resourceUrl.getPath());
        assertEquals(6,results.size());

    }

}
