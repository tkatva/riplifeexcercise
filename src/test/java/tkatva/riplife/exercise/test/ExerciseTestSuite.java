package tkatva.riplife.exercise.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
/**
 * Created by Tuomas Katva on 8.6.2014.
 */
@RunWith(Suite.class)
@SuiteClasses({TestPersistence.class, TestParsers.class})
public class ExerciseTestSuite {
}
