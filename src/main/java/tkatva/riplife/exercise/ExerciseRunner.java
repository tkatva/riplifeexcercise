package tkatva.riplife.exercise;

import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.parsers.ActionHandler;
import tkatva.riplife.exercise.parsers.ParserFactory;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.persistence.ShapeStore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.awt.geom.Area;


/**
 * Created by Tuomas Katva on 6.6.2014.
 *
 * Main class of Riplife exercise.
 *
 * I have used Java 8 in this project, maybe not entirely optimally but I would like learn it
 * and this exercise is good way get some training on that part too.
 * Also geometry is not my strong point, so I hope that it is not necessary in the position I am applying to. :)
 *
 * Sorry, I had to work on weekend so this exercise wasn't the best one. I should have created a lot more test
 * for example Commands were not tested at all and most of the parsers was not tested at all. I should have created
 * this exercise with TDD but it would have been to time consuming.
 *
 * TODO: If have time, refactor factories away and use Spring. Should also refactor duplicate Shape properties from point checker and Shape subclasses
 * TODO: Refactor ShapeCommands to Template method, too much duplicate code
 */
public class ExerciseRunner {

    private static final String WELCOME_MESSAGE = "Welcome Riplife excercise by Tuomas Katva, please enter command. Type \"help\" for instructions.";

    private static final String EXIT_MESSAGE = "Exit command given, exiting....";

    private static final String SHUTDOWN_SUCCESS_MSG = "Persistence store shutdown successful...";

    private static final String SHUTDOWN_EXCEPTION = "Exception shutting down persistence store : ";

    public static void main(String [] args)
    {
        //Bootstrap Hazelcast when starting
        PersistenceStoreFactory.getDefaultShapeStore();
        System.out.println(WELCOME_MESSAGE);
        ActionHandler handler = new ActionHandler();

        String commandStr = "";
        boolean doContinue = true;
        while (doContinue) {

            Scanner scanIn = new Scanner(System.in);
            commandStr = scanIn.nextLine();

            List<ActionResult> results = handler.processAction(commandStr);

            for(ActionResult result : results) {

                if (!result.isContinueProg()) {
                    doContinue = false;
                }


                System.out.println(result.getMsg());

            }

        }

        try {
            PersistenceStoreFactory.getDefaultShapeStore().shutDown();
            System.out.println(SHUTDOWN_SUCCESS_MSG);
        }catch (Exception exp) {
            System.out.println(SHUTDOWN_EXCEPTION  + exp.toString());
        }
        System.out.println(EXIT_MESSAGE);

    }

}
