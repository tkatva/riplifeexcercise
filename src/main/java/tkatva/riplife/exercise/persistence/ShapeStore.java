package tkatva.riplife.exercise.persistence;


import tkatva.riplife.exercise.commands.crud.CrudResults;
import tkatva.riplife.exercise.shapes.Shape;

import java.util.List;
import java.util.Optional;

/**
 * Created by Tuomas on 7.6.2014.
 */
public interface ShapeStore {

    CrudResults addShape(Shape shape);

    CrudResults updateShape(Shape shape);

    CrudResults removeShape(Shape shape);

    List<Shape> findShapesWithYandYcoordinates(double x, double y);

    Optional<Shape> findShapeById(String shapeId);

    List<Shape> findAll();

    void shutDown();

}
