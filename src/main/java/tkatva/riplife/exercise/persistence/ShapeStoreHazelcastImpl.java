package tkatva.riplife.exercise.persistence;

import tkatva.riplife.exercise.commands.crud.CrudResults;
import tkatva.riplife.exercise.shapes.Shape;
import com.hazelcast.core.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Tuomas Katva on 7.6.2014.
 *
 * Hazelcast ShapeStore implementation, this class persists Shapes to Hazelcast cluster
 * You even could define persistence store, so you could persist these precious Shapes to disk even :)
 *
 */
public class ShapeStoreHazelcastImpl implements ShapeStore {

    HazelcastInstance hazelcastInstance;
    private final String SHAPE_MAP_NAME = "shapes";
    private IMap<String,Shape> shapeMap;

    public ShapeStoreHazelcastImpl() {

        hazelcastInstance = Hazelcast.newHazelcastInstance();



        shapeMap = hazelcastInstance.getMap(SHAPE_MAP_NAME);

    }

    @Override
    public void shutDown() {
        hazelcastInstance.shutdown();
    }

    @Override
    public CrudResults addShape(Shape shape) {

        if (shape.getShapeId() != null) {
            shapeMap.put(shape.getShapeId(), shape);

            return CrudResults.SUCCESS;
        } else {
            return CrudResults.KEY_MISSING_FAILURE;
        }
    }

    @Override
    public CrudResults updateShape(Shape shape) {
        if (shape.getShapeId() != null) {
            shapeMap.lock(shape.getShapeId());
            try {

                shapeMap.remove(shape.getShapeId());
                shapeMap.put(shape.getShapeId(), shape);
                shapeMap.unlock(shape.getShapeId());

            } catch (Exception exp) {

            } finally {
                shapeMap.unlock(shape.getShapeId());
            }



            return CrudResults.SUCCESS;
        } else {
            return CrudResults.KEY_MISSING_FAILURE;
        }
    }

    @Override
    public CrudResults removeShape(Shape shape) {
        if (shape.getShapeId() != null) {

            shapeMap.remove(shape.getShapeId());
            return CrudResults.SUCCESS;

        } else {
            return CrudResults.KEY_MISSING_FAILURE;
        }
    }


    @Override
    public List<Shape> findAll() {
        return new ArrayList<>(shapeMap.values());
    }

    @Override
    public List<Shape> findShapesWithYandYcoordinates(double x, double y) {

        //This could be much more performant, maybe done in Hazelcast
        List<Shape> foundShapes = shapeMap.values().parallelStream()
                .filter(shape -> shape.doesIncludePoint(x,y))
                .collect(Collectors.toList());

        return foundShapes;

    }

    @Override
    public Optional<Shape> findShapeById(String shapeId) {

        Shape foundShape =  shapeMap.get(shapeId);
        if (foundShape != null) {
          return Optional.of(foundShape);
        } else {
            return Optional.empty();
        }

    }
}
