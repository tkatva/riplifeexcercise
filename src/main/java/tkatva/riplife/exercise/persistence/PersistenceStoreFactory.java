package tkatva.riplife.exercise.persistence;

/**
 * Created by Tuomas Katva on 7.6.2014.
 */
public class PersistenceStoreFactory {

    private static ShapeStore shapeStore = null;

    public static ShapeStore getDefaultShapeStore() {

        if (shapeStore == null) {
            shapeStore = new ShapeStoreHazelcastImpl();
        }

        return shapeStore;
    }

}
