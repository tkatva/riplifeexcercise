package tkatva.riplife.exercise.validators;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public interface PointChecker {

    boolean doesIncludePoint(double x, double y);

}
