package tkatva.riplife.exercise.commands;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuomas Katva on 6.6.2014.
 *
 * This class lists all available commands
 *
 */
public class CommandLineCommandFactory {

        public static List<CommandAction> getCommands() {

            List<CommandAction> commands = new ArrayList<CommandAction>();

            commands.add(new CommandLineHelpAction());
            commands.add(new CommandLineExitAction());
            commands.add(new CommandLineStoreAction());
            commands.add(new CommandLineFindAction());
            commands.add(new CommandLineFindXYAction());
            commands.add(new CommandLineDeleteAction());
            commands.add(new CommandLineFileAction());
            commands.add(new CommandLineListAllAction());

            return commands;

        }

}
