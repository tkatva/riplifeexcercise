package tkatva.riplife.exercise.commands;


import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 7.6.2014.
 *
 * Command class to find shape with id
 *
 */
public class CommandLineFindAction implements CommandAction{

    private String FIND_COMMAND = "find";
    private String[] parameters;

    @Override
    public ActionResult execute() {
        Optional<String> validationMsg = validateParams();
        if (validationMsg.isPresent()) {
            return new ActionResult(validationMsg.get(),true);
        } else {
            Optional<Shape> foundShape = PersistenceStoreFactory.getDefaultShapeStore().findShapeById(getShapeId());
            if (foundShape.isPresent()) {
                return new ActionResult(foundShape.get().toString(),true);
            } else {
                return new ActionResult(getCouldNotFindMsg(),true);
            }
        }

    }

    private String getShapeId() {

        if (parameters.length > 1) {
            return parameters[1];
        } else {
            return "";
        }

    }

    private String getCouldNotFindMsg() {
        return "Could not find shape with id : " + getShapeId();
    }

    private Optional<String> validateParams() {

        if (parameters == null || parameters.length < 2) {
            return Optional.of(getFailureMsg());
        } else {
            return Optional.empty();
        }

    }

    private String getFailureMsg() {
        return "Please give shape id to find... ";
    }

    @Override
    public String getCommand() {
        return FIND_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }
}
