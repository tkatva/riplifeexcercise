package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.result.ActionResult;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class CommandLineUnknownCommand implements CommandAction {


    private final String UNKNOWN_COMMAND_TITLE = "Unknown command, available commands are :";

    @Override
    public ActionResult execute() {
        return new ActionResult(createUnknowCommandString(),true);
    }

    private String createUnknowCommandString() {
        StringBuilder sb = new StringBuilder();
        sb.append(UNKNOWN_COMMAND_TITLE);
        sb.append("\n");
        CommandLineCommandFactory.getCommands().forEach(cmd -> {
            sb.append(cmd.getCommand());
            sb.append("\n");
        });

        return sb.toString();
    }

    @Override
    public void setParameters(String[] parameters) {

    }

    @Override
    public String getCommand() {
        return null;
    }
}
