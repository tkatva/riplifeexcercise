package tkatva.riplife.exercise.commands;


import tkatva.riplife.exercise.commands.result.ActionResult;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public interface CommandAction {

       ActionResult execute();

       String getCommand();

       void setParameters(String[] parameters);



}
