package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.shapes.Shape;

/**
 * Created by Tuomas on 7.6.2014.
 *
 * This should be refactored to Abstract class for reducing code duplication
 *
 */
public interface ShapeActionCommand extends CommandAction {



    void setActionToPerform(CrudAction crudActionParam);

    default String performCrudAction(Shape shape,CrudAction actionToPerform) {

        String returnMsg = null;

        switch (actionToPerform.executeCrudAction(shape)) {
            case SUCCESS:
                returnMsg = "Added : " + shape.toString();
                break;

            case FAILURE:
                //TODO: More meaningful message why failed
                returnMsg = "Failed to add : " + shape.toString();

                break;

            default:

                returnMsg = "Unknown result from action with shape : " + shape.toString();

                break;

        }

        return returnMsg;

    }

}
