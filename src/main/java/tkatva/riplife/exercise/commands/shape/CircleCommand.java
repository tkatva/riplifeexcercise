package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.shapes.Circle;
import math.geom2d.conic.Circle2D;
import tkatva.riplife.exercise.shapes.Shape;
import tkatva.riplife.exercise.shapes.ShapeFactory;

import java.util.Optional;

/**
 * Created by Tuomas on 7.6.2014.
 *
 * Class to process circle commands
 *
 */
public class CircleCommand implements ShapeActionCommand {


    private final String CIRCLE_COMMAND = "circle";
    private String[] parameters;
    private CrudAction actionToPerform;

    private double x;
    private double y;
    private double radius;

    private final String DEFAULT_TOO_FOW_PARAMS_MSG = "Too few parameters, example command : \"store circle 1.7 -5.05 6.9\"";
    private final String DEFAULT_PARSE_FAILURE_MSG = "Failed to parse circle x, y or radius, check that you have entered numbers";

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {
        this.actionToPerform = crudActionParam;
    }

    @Override
    public ActionResult execute() {
        Optional<String> validationMsgs = validateParams();
        if (validationMsgs.isPresent()) {
            return new ActionResult(validationMsgs.get(),true);
        } else {


            return new ActionResult(performCrudAction(ShapeFactory.getCircle(x,y,radius),actionToPerform),true);
        }
    }


    private Optional<String> validateParams()  {

        //Circle must have in total 5 parameters, for example "store circle 1.7 -5.05 6.9"
        if (parameters.length < 5) {
            return Optional.of(getTooFewParamsMsg());
        }
        return Optional.ofNullable(tryToParseDoubles());
    }

    private String tryToParseDoubles() {
        try {
            x = new Double(parameters[2]);
            y = new Double(parameters[3]);
            radius = new Double(parameters[4]);
            return null;
        } catch (Exception exp) {
            return getParseFailureMsg();
        }
    }

    @Override
    public String getCommand() {
        return CIRCLE_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }

    //Added this method so you could get localized messages with little refactoring :)
    private String getTooFewParamsMsg() {
        return DEFAULT_TOO_FOW_PARAMS_MSG;
    }

    private String getParseFailureMsg() {
        return DEFAULT_PARSE_FAILURE_MSG;
    }

    private String getSuccessMsg() {
        return "";
    }
}
