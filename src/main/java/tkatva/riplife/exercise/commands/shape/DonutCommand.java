package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.commands.shape.ShapeActionCommand;
import tkatva.riplife.exercise.shapes.ShapeFactory;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class DonutCommand implements ShapeActionCommand {


    private final String DONUT_COMMAND = "donut";
    private String[] parameters;
    private CrudAction actionToPerform;

    private double x;
    private double y;
    private double radius1;
    private double radius2;

    private final String DEFAULT_TOO_FOW_PARAMS_MSG = "Too few parameters, example command : \"store donut 1.7 -5.05 6.9 7.9\"";
    private final String DEFAULT_PARSE_FAILURE_MSG = "Failed to parse circle x, y or radius, check that you have entered numbers";

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {
        actionToPerform = crudActionParam;
    }

    private Optional<String> validateParams()  {

        //Donut must have in total 6 parameters, for example "store donut 1.7 -5.05 6.9 7.9"
        if (parameters.length < 6) {
            return Optional.of(getTooFewParamsMsg());
        }
        return Optional.ofNullable(tryToParseDoubles());
    }

    private String tryToParseDoubles() {
        try {
            x = new Double(parameters[2]);
            y = new Double(parameters[3]);
            radius1 = new Double(parameters[4]);
            radius2 = new Double(parameters[5]);
            return null;
        } catch (Exception exp) {
            return getParseFailureMsg();
        }
    }

    @Override
    public ActionResult execute() {
        Optional<String> validationMsgs = validateParams();
        if (validationMsgs.isPresent()) {
            return new ActionResult(validationMsgs.get(),true);
        } else {


            return new ActionResult(performCrudAction(ShapeFactory.getDonut(x, y, radius1, radius2),actionToPerform),true);
        }
    }

    @Override
    public String getCommand() {
        return DONUT_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {
        this.parameters = parametersParam;
    }


    private String getTooFewParamsMsg() {
        return DEFAULT_TOO_FOW_PARAMS_MSG;
    }

    private String getParseFailureMsg() {
        return DEFAULT_PARSE_FAILURE_MSG;
    }
}
