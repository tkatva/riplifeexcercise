package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.shapes.ShapeFactory;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class RectangleCommand implements ShapeActionCommand {

    private final String RECTANGLE_COMMAND = "rectangle";
    private String[] parameters;
    private CrudAction actionToPerform;

    private double x;
    private double y;
    private double side1;
    private double side2;

    private final String CHECK_RECTANGLE_PARAMETERS = "Check your rectangle parameters, example \"rectangle 3.5 2.0 5.6 7.2\"";

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {
            actionToPerform = crudActionParam;
    }

    @Override
    public ActionResult execute() {
        Optional<String> validationmsgs = validateParams();
        if (validationmsgs.isPresent()) {
            return new ActionResult(validationmsgs.get(),true);
        } else {
            return new ActionResult(performCrudAction(ShapeFactory.getRectangle(x,y,side1,side2),actionToPerform),true);
        }
    }

    private Optional<String> validateParams() {

        if(parameters == null || parameters.length != 6) {
           return Optional.of(getCheckParamsMsg());
        }
        Optional<String> parseResult = parseCoordinates();
        if(parseResult.isPresent()) {
           return Optional.of(parseResult.get());
        }
        return Optional.empty();
    }

    private Optional<String> parseCoordinates() {

        try {

            x = new Double(parameters[2]);
            y = new Double(parameters[3]);
            side1 = new Double(parameters[4]);
            side2 = new Double(parameters[5]);
            return Optional.empty();
        } catch (Exception exp) {
            return Optional.of("Failed to parse rectangle parameters");
        }

    }

    private String getCheckParamsMsg() {
        return CHECK_RECTANGLE_PARAMETERS;
    }

    @Override
    public String getCommand() {
        return RECTANGLE_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {
        this.parameters = parametersParam;
    }
}
