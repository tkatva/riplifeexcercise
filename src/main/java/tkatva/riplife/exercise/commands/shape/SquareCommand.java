package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.shapes.ShapeFactory;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class SquareCommand implements ShapeActionCommand {

    double x;
    double y;
    double side;

    private final String RECTANGLE_COMMAND = "square";
    private String[] parameters;
    private CrudAction actionToPerform;

    private final String CHECK_SQUARE_PARAMETERS = "Check your square parameters, example \"square 3.5 2.0 5.6\"";

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {
         actionToPerform = crudActionParam;
    }

    @Override
    public ActionResult execute() {
        Optional<String> validationmsgs = validateParams();
        if (validationmsgs.isPresent()) {
            return new ActionResult(validationmsgs.get(),true);
        } else {
            return new ActionResult(performCrudAction(ShapeFactory.getSquare(x, y, side),actionToPerform),true);
        }
    }

    private Optional<String> parseCoordinates() {

        try {

            x = new Double(parameters[2]);
            y = new Double(parameters[3]);
            side = new Double(parameters[4]);
            return Optional.empty();
        } catch (Exception exp) {
            return Optional.of("Failed to parse rectangle parameters");
        }

    }

    private String getCheckParamsMsg() {
        return CHECK_SQUARE_PARAMETERS;
    }

    private Optional<String> validateParams() {

        if(parameters == null || parameters.length != 5) {
            return Optional.of(getCheckParamsMsg());
        }
        Optional<String> parseResult = parseCoordinates();
        if(parseResult.isPresent()) {
            return Optional.of(parseResult.get());
        }
        return Optional.empty();
    }

    @Override
    public String getCommand() {
        return RECTANGLE_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {
        this.parameters = parametersParam;
    }
}
