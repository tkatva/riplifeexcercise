package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;

/**
 * Created by Tuomas on 7.6.2014.
 */
public class UnknownShapeCommand implements ShapeActionCommand {

    private final String DEFAULT_UNKNOWN_SHAPE_COMMAND_MSG = "Unknown shape, following shapes are available: ";
    @Override
    public ActionResult execute() {
        return new ActionResult(createUnknownShapeMsg(),true);
    }

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {

    }

    private String getUnknownMsgTitle() {
        return DEFAULT_UNKNOWN_SHAPE_COMMAND_MSG;
    }

    private String createUnknownShapeMsg() {

        StringBuilder sb = new StringBuilder();
        sb.append(getUnknownMsgTitle());
        sb.append("\n");
        ShapeCommandFactory.getAllShapeCommands().forEach(cmd -> {
            sb.append(cmd.getCommand());
            sb.append("\n");
        });

        return sb.toString();
    }

    @Override
    public String getCommand() {
        return null;
    }

    @Override
    public void setParameters(String[] parameters) {

    }
}
