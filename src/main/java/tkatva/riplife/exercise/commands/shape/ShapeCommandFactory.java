package tkatva.riplife.exercise.commands.shape;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuomas on 7.6.2014.
 *
 * This class lists all possible Shape manipulation commands
 *
 * To add new Shape to be persisted add Shape subclass and create command for
 * parsing parameters and executing what ever action is defined
 *
 */
public class ShapeCommandFactory {

    public static List<ShapeActionCommand> getAllShapeCommands() {
        List<ShapeActionCommand> shapeActions = new ArrayList<>();

        shapeActions.add(new CircleCommand());
        shapeActions.add(new RectangleCommand());
        shapeActions.add(new SquareCommand());
        shapeActions.add(new DonutCommand());
        shapeActions.add(new TriangleCommand());

        return shapeActions;
    }

}
