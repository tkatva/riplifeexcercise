package tkatva.riplife.exercise.commands.shape;

import tkatva.riplife.exercise.commands.crud.CrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.shapes.ShapeFactory;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class TriangleCommand implements ShapeActionCommand {


    private final String TRIANGLE_COMMAND = "triangle";
    private String[] parameters;
    private CrudAction actionToPerform;

    private double[] verticePoints = new double[6];

    private final String DEFAULT_TOO_FOW_PARAMS_MSG = "Too few parameters, example command : \"store triangle 4.5 1 -2.5 -33 23 0.3\"";
    private final String DEFAULT_PARSE_FAILURE_MSG = "Failed to parse circle x, y coordinates, check that you have entered numbers";


    private Optional<String> validateParams()  {


        if (parameters.length < 8) {
            return Optional.of(getTooFewParamsMsg());
        }
        return Optional.ofNullable(tryToParseDoubles());
    }

    private String tryToParseDoubles() {
        try {
            verticePoints[0] = new Double(parameters[2]);
            verticePoints[1] = new Double(parameters[3]);
            verticePoints[2] = new Double(parameters[4]);
            verticePoints[3] = new Double(parameters[5]);
            verticePoints[4] = new Double(parameters[6]);
            verticePoints[5] = new Double(parameters[7]);
            return null;
        } catch (Exception exp) {
            return getParseFailureMsg();
        }
    }

    @Override
    public void setActionToPerform(CrudAction crudActionParam) {
        this.actionToPerform = crudActionParam;
    }

    @Override
    public ActionResult execute() {
        Optional<String> validationMsgs = validateParams();
        if (validationMsgs.isPresent()) {
            return new ActionResult(validationMsgs.get(),true);
        } else {


            return new ActionResult(performCrudAction(
                    ShapeFactory.getTriangle(verticePoints[0],verticePoints[1],
                            verticePoints[2],verticePoints[3],
                            verticePoints[4],verticePoints[5]),actionToPerform),true);
        }
    }

    @Override
    public String getCommand() {
        return TRIANGLE_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }

    private String getTooFewParamsMsg() {
        return DEFAULT_TOO_FOW_PARAMS_MSG;
    }

    private String getParseFailureMsg() {
        return DEFAULT_PARSE_FAILURE_MSG;
    }
}
