package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.result.ActionResult;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class CommandLineHelpAction implements CommandAction {

    private final String HELP_COMMAND = "help";
    private final String HELP_COMMAND_MSG = "Available commands are: ";

    @Override
    public ActionResult execute() {

        return new ActionResult(createHelpString(),true);

    }

    @Override
    public void setParameters(String[] parameters) {

    }

    private String createHelpString() {

        StringBuilder sb = new StringBuilder();

        sb.append(HELP_COMMAND_MSG);
        sb.append("\n");

        CommandLineCommandFactory.getCommands().forEach( cmd -> {
            sb.append(cmd.getCommand());
            sb.append("\n");
        });

        return sb.toString();
    }

    @Override
    public String getCommand() {

        return HELP_COMMAND;
    }


}
