package tkatva.riplife.exercise.commands.result;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class ActionResult {



    private String msg;
    private boolean continueProg;

    public ActionResult(String msgParam, boolean continueProgParam) {
        msg = msgParam;
        continueProg = continueProgParam;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isContinueProg() {
        return continueProg;
    }
}
