package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.parsers.FileHandler;
import tkatva.riplife.exercise.parsers.FileHandlerFactory;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class CommandLineFileAction implements CommandAction {

    private String[] parameters;
    private final String FILE_ACTION = "file";
    private final String TEST_FILE_ACTION = "testfile";
    private final String TEST_FILE_NAME = "/testfile.txt";

    @Override
    public ActionResult execute() {

        FileHandler fileHandler = FileHandlerFactory.getDefaultFileHandler();

        Optional<String> validationMsg = validateParams();
        if (validationMsg.isPresent()) {
            return new ActionResult(validationMsg.get(),true);
        } else {
            List<ActionResult> results = null;
            if (parameters[1].trim().equals(TEST_FILE_ACTION)) {
                URL resourceUrl = getClass().
                        getResource(TEST_FILE_NAME);
                results = fileHandler.processFile(resourceUrl.getPath());

            } else {
               results  = fileHandler.processFile(parameters[1]);
            }

            return new ActionResult(createMessageFromResults(results),true);
        }
    }

    private String createMessageFromResults(List<ActionResult> results) {
        StringBuilder sb = new StringBuilder();

        sb.append("File processing results : \n" );
        results.forEach(result -> sb.append(result.getMsg() + "\n"));

        return sb.toString();
    }

    @Override
    public String getCommand() {
        return FILE_ACTION;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }

    private Optional<String> validateParams() {

        if (parameters == null || parameters.length < 2) {
            return Optional.of(getFailureMsg());
        } else {

            if(!testFile(parameters[1])) {
                return Optional.of(fileNotFoundMessage(parameters[1]));
            }

            return Optional.empty();
        }

    }

    private boolean testFile(String filename) {
        if(!filename.trim().equals(TEST_FILE_ACTION)) {
           File file = new File(filename);
           return file.exists();
        } else {
            return true;
        }
    }

    private String fileNotFoundMessage(String fileName) {
        return "Given file was not found : " + fileName;
    }

    private String getFailureMsg() {
        return "Please give filename to process or 'testfile'-command to process test file... ";
    }
}
