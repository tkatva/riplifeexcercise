package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.result.ActionResult;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class CommandLineExitAction implements  CommandAction {

    private final String EXIT_COMMAND = "exit";

    @Override
    public ActionResult execute() {
        return new ActionResult("exiting...",false);

    }

    @Override
    public String getCommand() {
        return EXIT_COMMAND;
    }

    @Override
    public void setParameters(String[] parameters) {

    }
}
