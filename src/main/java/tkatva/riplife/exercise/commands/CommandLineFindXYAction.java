package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;

import java.util.List;
import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 *
 * Command class to find shapes with x y coordinates
 *
 */
public class CommandLineFindXYAction implements CommandAction {

    private String FINDXY_COMMAND = "findxy";
    private String[] parameters;

    private double x;
    private double y;

    @Override
    public ActionResult execute() {

        Optional<String> validationMsg = validateParams();

        if(validationMsg.isPresent()) {
            return new ActionResult(validationMsg.get(),true);
        } else {
            return new ActionResult(getShapesAsString(PersistenceStoreFactory.getDefaultShapeStore().findShapesWithYandYcoordinates(x,y)),true);
        }

    }

    private String getShapesAsString(List<Shape> shapes) {

        StringBuilder sb = new StringBuilder();

        sb.append("Found shapes are :  \n");
        shapes.forEach(shape -> sb.append(shape.toString() + " shape surface area : " + shape.getSurfaceArea() + " \n"));

        double surfaceAreaSum = shapes.stream()
                .mapToDouble(Shape::getSurfaceArea)
                .sum();

        sb.append("Total surface area of shapes : " + surfaceAreaSum);

        return sb.toString();

    }


    private Optional<String> validateParams() {

        if (parameters == null || parameters.length < 3) {
            return Optional.of(getFailureMsg());
        } else {
            if (!tryToParseCoordinates()) {
                return Optional.of(getParseFailureMsg());
            }
            return Optional.empty();
        }

    }

    private boolean tryToParseCoordinates() {

        try {

            x = new Double(parameters[1]);
            y = new Double(parameters[2]);

            return true;
        } catch (Exception exp) {

            return false;

        }

    }

    private String getParseFailureMsg() {

        return "Please give x y coordinates as a number...";

    }

    private String getFailureMsg() {
        return "Please give x y coordinates to search...";
    }

    @Override
    public String getCommand() {
        return FINDXY_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }
}
