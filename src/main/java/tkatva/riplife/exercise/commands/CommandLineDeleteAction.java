package tkatva.riplife.exercise.commands;

import sun.security.provider.SHA;
import tkatva.riplife.exercise.commands.crud.CrudResults;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class CommandLineDeleteAction implements CommandAction {

    private String DELETE_ACTION = "delete";
    private String[] parameters;


    @Override
    public ActionResult execute() {
        Optional<String> validationMsg = validateParams();
        if (validationMsg.isPresent()) {
            return new ActionResult(validationMsg.get(),true);
        } else {

            Optional<Shape> foundShape = PersistenceStoreFactory.getDefaultShapeStore().findShapeById(getShapeId());
            if (foundShape.isPresent()) {
                Shape shapeToRemove = foundShape.get();
                CrudResults result = PersistenceStoreFactory.getDefaultShapeStore().removeShape(shapeToRemove);
                if (result.equals(CrudResults.SUCCESS)) {
                  return new ActionResult(getSuccessMsg(shapeToRemove),true);
                } else {
                   return new ActionResult(unknownFailureMsg(shapeToRemove),true);
                }
            } else {
                return new ActionResult(getCouldNotFindMsg(),true);
            }
        }
    }

    private Optional<String> validateParams() {

        if (parameters == null || parameters.length < 2) {
            return Optional.of(getFailureMsg());
        } else {
            return Optional.empty();
        }

    }

    @Override
    public String getCommand() {
        return DELETE_ACTION;
    }

    @Override
    public void setParameters(String[] parametersParam) {
        this.parameters = parametersParam;
    }

    private String getCouldNotFindMsg() {
        return "Could not find shape with id : " + getShapeId();
    }

    private String getShapeId() {

        if (parameters.length > 1) {
            return parameters[1];
        } else {
            return "";
        }

    }

    private String getSuccessMsg(Shape shape) {
        return "Removed shape : " + shape.toString();
    }

    private String unknownFailureMsg(Shape shape) {
        return "Unknown error when removing : " + shape.toString();
    }

    private String getFailureMsg() {
        return "Please give shape id to find... ";
    }
}
