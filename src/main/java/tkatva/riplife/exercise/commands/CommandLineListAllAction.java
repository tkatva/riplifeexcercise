package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;

import java.util.List;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class CommandLineListAllAction implements CommandAction {


    private String FIND_COMMAND = "findall";
    private String[] parameters;


    @Override
    public ActionResult execute() {
       List<Shape> shapeList = PersistenceStoreFactory.getDefaultShapeStore().findAll();
       return new ActionResult(printAll(shapeList),true);
    }

    private String printAll(List<Shape> shapes) {
        StringBuilder sb = new StringBuilder();

        sb.append("All stored shapes : \n");
        shapes.parallelStream()
                .forEach(shape -> sb.append(shape.toString() +" \n"));

        return sb.toString();
    }

    @Override
    public String getCommand() {
        return FIND_COMMAND;
    }

    @Override
    public void setParameters(String[] parametersParam) {

        this.parameters = parametersParam;

    }
}
