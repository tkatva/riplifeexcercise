package tkatva.riplife.exercise.commands.crud;

/**
 * Created by Tuomas on 7.6.2014.
 */
public enum CrudResults {

    SUCCESS(1),FAILURE(-1),KEY_MISSING_FAILURE(-2);

    private int value;

    private CrudResults(int valueParam) {
        this.value = valueParam;
    }

}
