package tkatva.riplife.exercise.commands.crud;

import tkatva.riplife.exercise.shapes.Shape;

/**
 * Created by Tuomas on 7.6.2014.
 */
public interface CrudAction {

    CrudResults executeCrudAction(Shape shape);
}
