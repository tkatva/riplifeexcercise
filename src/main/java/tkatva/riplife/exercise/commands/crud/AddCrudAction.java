package tkatva.riplife.exercise.commands.crud;


import tkatva.riplife.exercise.persistence.PersistenceStoreFactory;
import tkatva.riplife.exercise.shapes.Shape;
import tkatva.riplife.exercise.shapes.ShapeFactory;

/**
 * Created by Tuomas on 7.6.2014.
 */
public class AddCrudAction implements CrudAction {

    @Override
    public CrudResults executeCrudAction(Shape shape) {



        return PersistenceStoreFactory.getDefaultShapeStore().addShape(shape);
    }
}
