package tkatva.riplife.exercise.commands;

import tkatva.riplife.exercise.commands.crud.AddCrudAction;
import tkatva.riplife.exercise.commands.result.ActionResult;
import tkatva.riplife.exercise.commands.shape.ShapeActionCommand;
import tkatva.riplife.exercise.commands.shape.ShapeCommandFactory;
import tkatva.riplife.exercise.commands.shape.UnknownShapeCommand;

/**
 * Created by Tuomas on 7.6.2014.
 *
 * This command adds shapes to repository. If no matching shape command is found it
 * returns UnknownShapeCommand
 *
 */
public class CommandLineStoreAction implements CommandAction {

    private String[] parameters;
    private final String STORE_ACTION = "store";

    @Override
    public ActionResult execute() {

        ShapeActionCommand cmda = null;

        for(ShapeActionCommand cmd: ShapeCommandFactory.getAllShapeCommands()) {
            if (parameters.length > 1 && cmd.getCommand().trim().equalsIgnoreCase(parameters[1])) {
                cmda = cmd;
            }
        }

        if (cmda == null) {
            cmda = new UnknownShapeCommand();
        } else {
            cmda.setParameters(parameters);
            cmda.setActionToPerform(new AddCrudAction());
        }

        return cmda.execute();
    }

    @Override
    public String getCommand() {
        return STORE_ACTION;
    }

    @Override
    public void setParameters(String[] parametersParam) {
        this.parameters = parametersParam;

    }
}
