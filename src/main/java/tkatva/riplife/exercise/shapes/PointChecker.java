package tkatva.riplife.exercise.shapes;

import java.io.Serializable;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public interface PointChecker extends Serializable {

    boolean doesIncludePoint(double x, double y);

}
