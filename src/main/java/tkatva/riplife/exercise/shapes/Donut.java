package tkatva.riplife.exercise.shapes;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class Donut extends Shape {

    private double innerRadius;
    private double outerRadius;

    public Donut(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {
        super(pointCheckerParam, shapeIdParam);
    }

    @Override
    public boolean doesIncludePoint(double searcX, double searchY) {
        return super.doesIncludePoint(searcX, searchY);
    }

    @Override
    public double getSurfaceArea() {
        return Math.PI * (outerRadius * outerRadius);
    }

    @Override
    public String getShapeId() {
        return super.getShapeId();
    }

    @Override
    public String toString() {

        return " shape : " + this.getShapeId() + " donut -> x :  " + getX() + " y : " + getY() + " inner radius :  " + innerRadius + " outer radius : " + outerRadius;
    }

    public double getInnerRadius() {
        return innerRadius;
    }

    public void setInnerRadius(double innerRadius) {
        this.innerRadius = innerRadius;
    }

    public double getOuterRadius() {
        return outerRadius;
    }

    public void setOuterRadius(double outerRadius) {
        this.outerRadius = outerRadius;
    }


}
