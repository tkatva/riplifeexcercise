package tkatva.riplife.exercise.shapes;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class Square extends Shape {

    private double side;

    public Square(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {
        super(pointCheckerParam, shapeIdParam);
    }

    @Override
    public boolean doesIncludePoint(double searcX, double searchY) {
        return super.doesIncludePoint(searcX, searchY);
    }

    @Override
    public double getSurfaceArea() {
        return side*side;
    }

    @Override
    public String getShapeId() {
        return super.getShapeId();
    }

    @Override
    public String toString() {
        return " shape : " + this.getShapeId() + " square -> x :  " + getX() + " y : " + getY() + " side :  " + side;

    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
