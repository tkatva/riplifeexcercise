package tkatva.riplife.exercise.shapes;

import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public abstract class Shape implements Serializable {

    PointChecker pointChecker;

    String shapeId;

    private double x;
    private double y;

    public Shape(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {

        this.pointChecker = pointCheckerParam;

        if(shapeIdParam.isPresent()) {
            shapeId = shapeIdParam.get();
        } else {
            shapeId = createShapeId();
        }
    }

    private String createShapeId() {
        return   UUID.randomUUID().toString();
    }

   public boolean doesIncludePoint(double searcX , double searchY) {
       return pointChecker.doesIncludePoint(searcX,searchY);
   }

   public abstract double getSurfaceArea();

    public String getShapeId() {

        return shapeId;

    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
