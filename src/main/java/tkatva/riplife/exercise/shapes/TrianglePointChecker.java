package tkatva.riplife.exercise.shapes;

import  math.geom2d.Point2D;
import math.geom2d.polygon.Polygon2D;
import math.geom2d.polygon.SimplePolygon2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class TrianglePointChecker implements PointChecker {


    private double vertice1X;
    private double vertice1Y;
    private double vertice2X;
    private double vertice2Y;
    private double vertice3X;
    private double vertice3Y;

    public TrianglePointChecker(double vertice1XParam,double vertice1YParam,double vertice2XParam,double vertice2YParam,
                                double vertice3XParam, double vertice3YParam) {

        this.vertice1X = vertice1XParam;
        this.vertice1Y = vertice1YParam;
        this.vertice2X = vertice2XParam;
        this.vertice2Y = vertice2YParam;
        this.vertice3X = vertice3XParam;
        this.vertice3Y = vertice3YParam;

    }

    private Point2D getPoint(double x,double y) {
        return new Point2D(x,y);
    }

    @Override
    public boolean doesIncludePoint(double x, double y) {
        List<Point2D> points = new ArrayList<>();
        points.add(getPoint(this.vertice1X,this.vertice1Y));
        points.add(getPoint(this.vertice2X,this.vertice2Y));
        points.add(getPoint(this.vertice3X,this.vertice3Y));

        Polygon2D polygon2D = new SimplePolygon2D(points);
        return polygon2D.contains(x,y);
    }


}
