package tkatva.riplife.exercise.shapes;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 7.6.2014.
 */
public class Circle extends Shape {


    private double radius;

    public Circle(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {
        super(pointCheckerParam, shapeIdParam);
    }

    @Override
    public boolean doesIncludePoint(double searchX, double searchY) {
        return super.doesIncludePoint(searchX, searchY);
    }

    @Override
     public double getSurfaceArea() {
        return Math.PI * (radius * radius);
    }

    @Override
    public String getShapeId() {
        return super.getShapeId();
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return " shape : " + this.getShapeId() + " circle -> x :  " + getX() + " y : " + getY() + " radius :  " + radius;

    }
}
