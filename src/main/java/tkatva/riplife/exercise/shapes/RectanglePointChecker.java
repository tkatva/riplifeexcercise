package tkatva.riplife.exercise.shapes;

import java.io.Serializable;
import java.awt.geom.Rectangle2D;
/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class RectanglePointChecker implements PointChecker {

    double x;
    double y;
    double side1;
    double side2;

    public RectanglePointChecker(double xParam,double yParam, double side1Param, double side2Param) {
        this.x = xParam;
        this.y = yParam;
        this.side1 = side1Param;
        this.side2 = side2Param;
    }

    @Override
    public boolean doesIncludePoint(double xParam, double yParam) {

        Rectangle2D rect = new Rectangle2D.Double(x,y,side1,side2);
        boolean rectContains = rect.contains(xParam,yParam);
        return rectContains;
    }
}
