package tkatva.riplife.exercise.shapes;

import math.geom2d.Point2D;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 7.6.2014.
 *
 * Factory class for creating different kinds of shapes
 *
 */
public class ShapeFactory {

    /*
        Should remove duplicate values from point checkers and shapes if have enough time abd
        create own Serializable point class, too many parameters
     */

    public static Circle getCircle(double x, double y, double radius) {

        Circle circle = new Circle(new CirclePointChecker(x,y,radius), Optional.<String>empty());
        circle.setX(x);
        circle.setY(y);
        circle.setRadius(radius);

        return circle;

    }

    public static Rectangle getRectangle(double x, double y, double side1, double side2) {

        Rectangle rectangle = new Rectangle(new RectanglePointChecker(x,y,side1,side2),Optional.empty());
        rectangle.setX(x);
        rectangle.setY(y);
        rectangle.setSide1(side1);
        rectangle.setSide2(side2);
        return rectangle;
    }

    public static Square getSquare(double x, double y, double side) {

        Square square = new Square(new RectanglePointChecker(x,y,side,side),Optional.empty());
        square.setSide(side);
        square.setX(x);
        square.setY(y);

        return square;


    }

    public static Donut getDonut(double x, double y, double radius1, double radius2) {

        double innerRadius = radius1 < radius2 ? radius1 : radius2;
        double outerRadius = radius1 > radius2 ? radius1 : radius2;

        Donut donut = new Donut(new DonutPointChecker(x,y,innerRadius,outerRadius),Optional.empty());
        donut.setX(x);
        donut.setY(y);
        donut.setInnerRadius(innerRadius);
        donut.setOuterRadius(outerRadius);
        return  donut;

    }

    //TODO : This is too ugly refactor this to Serializable point class
    public static Triangle getTriangle(double vertice1X,double vertice1Y,double vertice2X,double vertice2Y,
                                       double vertice3X, double vertice3Y) {



        Triangle triangle = new Triangle(new TrianglePointChecker(vertice1X,vertice1Y,vertice2X,vertice2Y,vertice3X,vertice3Y),Optional.empty());
        triangle.setVertice1X(vertice1X);
        triangle.setVertice1Y(vertice1Y);
        triangle.setVertice2X(vertice2X);
        triangle.setVertice2Y(vertice2Y);
        triangle.setVertice3X(vertice3X);
        triangle.setVertice3Y(vertice3Y);

        return triangle;

    }

}
