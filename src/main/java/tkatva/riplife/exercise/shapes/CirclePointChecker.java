package tkatva.riplife.exercise.shapes;

import math.geom2d.conic.Circle2D;
import math.geom2d.Point2D;

import java.io.Serializable;

/**
 * Created by Tuomas Katva on 7.6.2014.
 */
public class CirclePointChecker implements PointChecker {


    double x, y, radius;

    public CirclePointChecker(double xParam, double yParam, double radiusParam)  {

        x = xParam;
        y = yParam;
        radius = radiusParam;

    }

    @Override
    public boolean doesIncludePoint(double searchX, double searchY) {
        Circle2D circle2D = new Circle2D(x,y,radius);
        Point2D point2D = new Point2D(searchX,searchY);
        boolean doesIncludePoint = circle2D.isInside(point2D);
        return doesIncludePoint;
    }
}
