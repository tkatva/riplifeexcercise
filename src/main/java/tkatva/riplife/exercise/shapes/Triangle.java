package tkatva.riplife.exercise.shapes;

import  math.geom2d.Point2D;
import  math.geom2d.polygon.SimplePolygon2D;
import  math.geom2d.polygon.Polygon2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class Triangle extends Shape {

    private double vertice1X;
    private double vertice1Y;
    private double vertice2X;
    private double vertice2Y;
    private double vertice3X;
    private double vertice3Y;

    public Triangle(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {
        super(pointCheckerParam, shapeIdParam);

    }

    @Override
    public boolean doesIncludePoint(double searcX, double searchY) {
        return super.doesIncludePoint(searcX, searchY);
    }

    private Point2D getPoint(double x,double y) {
        return new Point2D(x,y);
    }

    @Override
    public double getSurfaceArea() {

        List<Point2D> points = new ArrayList<>();
        points.add(getPoint(getVertice1X(), getVertice1Y()));
        points.add(getPoint(getVertice2X(), getVertice2Y()));
        points.add(getPoint(getVertice3X(), getVertice3Y()));

        Polygon2D polygon2D = new SimplePolygon2D(points);

        return polygon2D.area();
    }

    @Override
    public String getShapeId() {
        return super.getShapeId();
    }

    @Override
    public String toString() {

        return " shape : " + this.getShapeId() + " triangle -> vertice1 x :  " + vertice1X + " vertice1 y : " + vertice1X +
                "vertice2 x :  " + vertice2X + " vertice2 y : " + vertice2X +
                "vertice3 x :  " + vertice3X + " vertice3 y : " + vertice3X;

    }

    public double getVertice1X() {
        return vertice1X;
    }

    public void setVertice1X(double vertice1X) {
        this.vertice1X = vertice1X;
    }

    public double getVertice1Y() {
        return vertice1Y;
    }

    public void setVertice1Y(double vertice1Y) {
        this.vertice1Y = vertice1Y;
    }

    public double getVertice2X() {
        return vertice2X;
    }

    public void setVertice2X(double vertice2X) {
        this.vertice2X = vertice2X;
    }

    public double getVertice2Y() {
        return vertice2Y;
    }

    public void setVertice2Y(double vertice2Y) {
        this.vertice2Y = vertice2Y;
    }

    public double getVertice3X() {
        return vertice3X;
    }

    public void setVertice3X(double vertice3X) {
        this.vertice3X = vertice3X;
    }

    public double getVertice3Y() {
        return vertice3Y;
    }

    public void setVertice3Y(double vertice3Y) {
        this.vertice3Y = vertice3Y;
    }


}
