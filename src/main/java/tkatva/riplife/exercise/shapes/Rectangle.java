package tkatva.riplife.exercise.shapes;

import java.util.Optional;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class Rectangle extends Shape {

    private double side1;
    private double side2;

    public Rectangle(PointChecker pointCheckerParam, Optional<String> shapeIdParam) {
        super(pointCheckerParam, shapeIdParam);
    }

    @Override
    public boolean doesIncludePoint(double searcX, double searchY) {
        return super.doesIncludePoint(searcX, searchY);
    }

    @Override
    public double getSurfaceArea() {
        return side1*side2;
    }

    @Override
    public String getShapeId() {
        return super.getShapeId();
    }

    public double getSide1() {
        return side1;
    }

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public double getSide2() {
        return side2;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    @Override
    public String toString() {
        return " shape : " + this.getShapeId() + " rectangle -> x :  " + getX() + " y : " + getY() + " side1 :  " + side1 + " side2 : " + side2;
    }
}
