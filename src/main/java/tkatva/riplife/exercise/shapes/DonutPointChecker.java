package tkatva.riplife.exercise.shapes;

import math.geom2d.Point2D;
import math.geom2d.conic.Circle2D;

import java.io.Serializable;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public class DonutPointChecker implements PointChecker {


    private double x;
    private double y;
    private double innerRadius;
    private double outerRadius;

    public DonutPointChecker(double xParam,double yParam, double innerRadiusParam, double outerRadiusParam) {

        x = xParam;
        y = yParam;
        innerRadius = innerRadiusParam;
        outerRadius = outerRadiusParam;

    }

    @Override
    public boolean doesIncludePoint(double xParam, double yParam) {

        Circle2D innerCircle = new Circle2D(x,y,innerRadius);

        Circle2D outerCircle = new Circle2D(x,y,outerRadius);

        boolean isInInnerCircle = innerCircle.isInside(new Point2D(xParam,yParam));
        boolean isInOuterCircle = outerCircle.isInside(new Point2D(xParam,yParam));

        return (!isInInnerCircle && isInOuterCircle);


    }
}
