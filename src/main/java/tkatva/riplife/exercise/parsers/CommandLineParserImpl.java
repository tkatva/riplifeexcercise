package tkatva.riplife.exercise.parsers;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.CommandLineCommandFactory;
import tkatva.riplife.exercise.commands.CommandLineUnknownCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class CommandLineParserImpl implements CommandLineParser {

    private InputStringParser inputStringParser = InputStringParserFactory.getDefaultInputStringParser();

    @Override
    public  List<CommandAction> parseCommand(String commandParam) {

        List<CommandAction> returnCommands = new ArrayList<>();

        String[] parameters = inputStringParser.parseInputString(commandParam);

        if (parameters != null && parameters.length > 0) {
            String firstCommand = parameters[0];

            CommandLineCommandFactory.getCommands().forEach(cmd -> {

                if (cmd.getCommand().trim().equalsIgnoreCase(firstCommand)) {
                    cmd.setParameters(parameters);
                    returnCommands.add(cmd);
                }

            });

            if (returnCommands.size() < 1) {
                returnCommands.add(new CommandLineUnknownCommand());
            }

        } else {
            returnCommands.add(new CommandLineUnknownCommand());
        }
        return returnCommands;
    }



}
