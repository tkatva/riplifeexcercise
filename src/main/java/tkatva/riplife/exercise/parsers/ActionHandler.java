package tkatva.riplife.exercise.parsers;

import tkatva.riplife.exercise.commands.CommandAction;
import tkatva.riplife.exercise.commands.CommandLineCommandFactory;
import tkatva.riplife.exercise.commands.result.ActionResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public class ActionHandler {

    public List<ActionResult> processAction(String actionStrParam) {

        List<CommandAction> actions = ParserFactory.getCommandLineParser().parseCommand(actionStrParam);

        List<ActionResult> actionResults = new ArrayList<>();

        actions.forEach(cmd -> {
            actionResults.add(cmd.execute());
        });

        return actionResults;

    }

}
