package tkatva.riplife.exercise.parsers;

/**
 * Created by Tuomas on 7.6.2014.
 */
public interface InputStringParser {

    String[] parseInputString(String inputString);

}
