package tkatva.riplife.exercise.parsers;

import tkatva.riplife.exercise.commands.result.ActionResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuomas Katva on 8.6.2014.
 *
 * This class reads commands from file and expects each command to be on a separate line.
 *
 */
public class DefaultFileHandler implements FileHandler {

    @Override
    public List<ActionResult> processFile(String fileName) {
        List<ActionResult> results = new ArrayList<>();
        ActionHandler actionHandler = new ActionHandler();
        try {
            Files.readAllLines(Paths.get(fileName)).parallelStream().forEach( command ->  results.addAll(actionHandler.processAction(command)));
        } catch (IOException e) {
            results.add(new ActionResult("Could not process file : " + e.toString(), true));
        }
        return results;
    }
}
