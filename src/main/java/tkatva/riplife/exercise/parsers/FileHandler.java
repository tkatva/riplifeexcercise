package tkatva.riplife.exercise.parsers;

import tkatva.riplife.exercise.commands.result.ActionResult;

import java.util.List;

/**
 * Created by Tuomas Katva on 8.6.2014.
 */
public interface FileHandler {

    List<ActionResult> processFile(String fileName);

}
