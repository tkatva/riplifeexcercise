package tkatva.riplife.exercise.parsers;

import tkatva.riplife.exercise.commands.CommandAction;

import java.util.List;

/**
 * Created by Tuomas Katva on 6.6.2014.
 */
public interface CommandLineParser {

    public List<CommandAction> parseCommand(String commandParam);

}
