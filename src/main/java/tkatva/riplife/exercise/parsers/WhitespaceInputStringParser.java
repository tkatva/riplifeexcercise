package tkatva.riplife.exercise.parsers;

import java.util.StringTokenizer;

/**
 * Created by Tuomas on 7.6.2014.
 */
public class WhitespaceInputStringParser implements InputStringParser {

    @Override
    public String[] parseInputString(String inputString) {
        String[] returnArray = null;

        StringTokenizer st = new StringTokenizer(inputString," ");
        returnArray = new String[st.countTokens()];
        int counter = 0;
        while (st.hasMoreElements()) {

            returnArray[counter] = st.nextToken();
            counter++;
        }

        return returnArray;
    }
}
