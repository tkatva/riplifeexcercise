Riplife exercise by Tuomas Katva. 8.6.2014.

 * I have used Java 8 in this project, maybe not entirely optimally but I would like learn it
 * and this exercise is good way get some training on that part too.
 * Also geometry is not my strong point, so I hope that it is not necessary in the position I am applying to. :)
 *
 * Sorry, I had to work on weekend so this exercise wasn't the best one. I should have created a lot more test
 * for example Commands were not tested at all and most of the parsers was not tested at all. I should have created
 * this exercise with TDD but it would have been to time consuming.